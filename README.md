# README #

###AnomalyFlag

This package uses Fast-fourier transform and the decompose function to identify seasonality and trend in a timeseries, and isolate the underlying error.  It will then highlight any anomalies in the error which violate a specified threshold in standard deviations.

### How do I get set up? ###

Download from this repo and then require(AnomalyFlag) in the normal way.

Depends: TSA, xts, dygraphs, lubridate

### Sample Output ###

![anomaly-sample-chart.png](https://bitbucket.org/repo/B7xLLj/images/289763959-anomaly-sample-chart.png)

### Who do I talk to? ###

* John O'Gorman John.OGorman@annalect.com
* Nigel Ng  Nigel.Ng@annalect.com